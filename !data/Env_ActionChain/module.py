import model.emb as emb
import typing


# ==============================
class CInnerState(emb.ISampleDetector):
    _step_mas: typing.List[emb.IDynamic]
    _sample_mas: typing.List[emb.CSampleDetector]
    _detector_mas: typing.List[emb.CDetector]

    def __init__(self):
        self._step_mas = []
        self._sample_mas = []
        self._detector_mas = []

        self._sample_mas.append(emb.CSampleDetector(
            i_out_mas=[
                [1, 0, 0, 0, 0]
            ],
            i_out_none=[0, 0, 0, 0, 0],
            i_is_start=lambda i_tick_idx, i_input: i_tick_idx == 0
        ))

        self._sample_mas.append(emb.CSampleDetector(
            i_out_mas=[
                [0, 1, 0, 0, 0]
            ],
            i_out_none=[0, 0, 0, 0, 0],
            i_is_start=lambda i_tick_idx, i_input: i_tick_idx == 6
        ))

        self._sample_mas.append(emb.CSampleDetector(
            i_out_mas=[
                [0, 0, 1, 0, 0]
            ],
            i_out_none=[0, 0, 0, 0, 0],
            i_is_start=lambda i_tick_idx, i_input: i_tick_idx == 12
        ))

        self._sample_mas.append(emb.CSampleDetector(
            i_out_mas=[
                [0, 0, 0, 1, 0],
                [1, 0, 0, 0, 0],
                [0, 1, 0, 0, 0],
                [0, 0, 0, 0, 0],
                [0, 0, 1, 0, 0]
            ],
            i_out_none=[0, 0, 0, 0, 0],
            i_is_start=lambda i_tick_idx, i_input: i_tick_idx == 18
        ))

        self._sample_mas.append(emb.CSampleDetector(
            i_out_mas=[
                [0, 0, 0, 1, 0]
            ],
            i_out_none=[0, 0, 0, 0, 0],
            i_is_start=lambda i_tick_idx, i_input: i_tick_idx == 30
        ))

        self._detector_mas.append(emb.CDetector(
            i_detect_mas=[
                emb.CDetectItem(1, 0, 0),
                emb.CDetectItem(0, 1, 0),
                emb.CDetectItem(0, 0, 0),
                emb.CDetectItem(0, 0, 1),
            ]
        ))

        self._sample_mas.append(emb.CSampleDetector(
            i_out_mas=[
                [0, 0, 0, 0, 1]
            ],
            i_out_none=[0, 0, 0, 0, 0],
            i_is_start=self._detector_mas[-1].is_start
        ))

        self._step_mas.extend(self._sample_mas)
        self._step_mas.extend(self._detector_mas)

    def is_start(self, i_tick_idx, i_input):
        r = map(
            lambda s: 1 if s.is_start(i_tick_idx, i_input) else 0,
            self._sample_mas
        )
        return list(r)

    def get_current(self):
        t = map(
            lambda s: s.get_current(), 
            self._sample_mas
        )
        return list(map(max, *t))

    def do_step(self):
        list(map(
            lambda s: s.do_step(),
            self._step_mas
        ))


# ==============================
class CModule(emb.IModule):
    def __init__(self):
        self.Param = {
            emb.State.Input: {"": 8},
            emb.State.Inner: {"Sample": 2},
            emb.State.Output: {"": 4, "Reward": 1}
        }

        self.State = CInnerState()

    def get_param(self):
        return self.Param

    def step(self, i_tick_idx, i_input):
        v_inner = self.State.is_start(i_tick_idx, i_input[""])
        v_output = self.State.get_current()
        v_reward = v_output[-1]

        self.State.do_step()

        return (
            {"Sample": v_inner},
            {"": v_output[0:-1], "Reward": [v_reward]}
        )

    def get_component(self):
        return None


# ==============================
def test(i_step=0):
    v_module = CModule()

    for i in range(10):
        if i == 5:
            v_in = [1, 0, 0, 0, 0, 0, 0, 0]
        else:
            v_in = [0, 0, 0, 0, 0, 0, 0, 0]
            
        print(v_module.step(i_step + i, {"": v_in}))


# ==============================
if __name__ == '__main__':
    test()
