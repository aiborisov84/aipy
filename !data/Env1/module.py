import model.emb as emb


# ==============================
class CInnerState(emb.ISampleDetector):
    def __init__(self):
        self.Sample = []
        self.Sample.append(emb.CSampleDetector(
            i_out_mas=[
                [0, 0, 0],
                [1, 1, 0],
                [0, 0, 1],
                [0, 1, 0],
            ],
            i_out_none=[0, 0, 0],
            i_is_start=lambda i_tick_idx, i_input: i_tick_idx % 20 == 0
        ))
        self.Detector = emb.CDetector(
            i_detect_mas=[
                emb.CDetectItem(1)
            ]
        )
        self.Sample.append(emb.CSampleDetector(
            i_out_mas=[
                [1, 1, 1]
            ],
            i_out_none=[0, 0, 0],
            i_is_start=self.Detector.is_start
        ))
        
    def is_start(self, i_tick_idx, i_input):
        r = map(
            lambda s: 1 if s.is_start(i_tick_idx, i_input) else 0,
            self.Sample
        )
        return list(r)

    def get_current(self):
        t = map(
            lambda s: s.get_current(), 
            self.Sample
        )
        return list(map(max, *t))

    def do_step(self):
        list(map(
            lambda s: s.do_step(), 
            self.Sample
        ))
        self.Detector.do_step()


# ==============================
class CModule(emb.IModule):
    def __init__(self):
        self.Param = {
            emb.State.Input: {"": 8},
            emb.State.Inner: {"Sample": 2},
            emb.State.Output: {"": 3, "Reward": 1}
        }

        self.State = CInnerState()

    def get_param(self):
        return self.Param

    def step(self, i_tick_idx, i_input):
        v_inner = self.State.is_start(i_tick_idx, i_input[""])
        v_output = self.State.get_current()
        v_reward = 1 if v_inner[1] > 0 else 0

        self.State.do_step()

        return (
            {"Sample": v_inner},
            {"": v_output, "Reward": [v_reward]}
        )

    def get_component(self):
        return None


# ==============================
def test(i_step=0):
    v_module = CModule()

    for i in range(10):
        if i == 5:
            v_in = [1, 0, 0, 0, 0, 0, 0, 0]
        else:
            v_in = [0, 0, 0, 0, 0, 0, 0, 0]
            
        print(v_module.step(i_step + i, {"": v_in}))


# ==============================
if __name__ == '__main__':
    test()
