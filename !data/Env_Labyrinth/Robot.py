from dataclasses import dataclass

import model.emb as emb


# ==============================
class CEnergy(emb.IDynamic):
    def __init__(self, i_max_energy, i_low_energy=None):
        self._MaxEnergy = i_max_energy

        if i_low_energy is not None:
            self._LowEnergy = i_low_energy
        else:
            self._LowEnergy = 2 * i_max_energy // 3

        self.Value = i_max_energy

    def do_step(self):
        self.sub(1)

    @property
    def is_low(self):
        return self.Value < self._LowEnergy

    def add(self, i_add_value):
        self.Value = self.Value + i_add_value
        if self._MaxEnergy < self.Value:
            self.Value = self._MaxEnergy

    def add_full(self):
        self.Value = self._MaxEnergy

    def sub(self, i_sub_value):
        self.Value = self.Value - i_sub_value
        if self.Value < 0:
            self.Value = 0

    def restart(self):
        self.Value = self._MaxEnergy


# ==============================
@dataclass
class CStateResult:
    Energy: int
    Hungry: bool


# ==============================
class State(emb.IDynamic):
    Energy: CEnergy

    def __init__(self, i_energy: CEnergy):
        self.Energy = i_energy

        self._step_mas = []
        self._step_mas.append(self.Energy)

    @property
    def is_hungry(self):
        return self.Energy.is_low

    def set_input(self, i_tick_idx, i_food_place, i_do_work, i_do_step):
        _ = i_tick_idx
        if i_food_place and not i_do_step:
            self.Energy.add_full()

        if i_do_step:
            self.Energy.sub(1)

        if i_do_work:
            self.Energy.sub(3)

        return CStateResult(self.Energy.Value, self.is_hungry)

    def do_step(self):
        list(map(
            lambda s: s.do_step(),
            self._step_mas
        ))

    def restart(self):
        self.Energy.restart()
