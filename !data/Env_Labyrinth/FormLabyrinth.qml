import QtQuick.Controls 2.0
import QtQuick 2.11
import QtQuick.Shapes 1.11


Page {
    anchors.fill: parent

    function min(i1, i2) {
        return i1 < i2 ? i1 : i2
    }

    function max(i1, i2) {
        return i1 < i2 ? i2 : i1
    }

    property bool isVisibleVertical: true
    property bool isVisibleHorisontal: true
    property bool isVisibleWork: true

    property int imgMargin: 10

    property int rowCount: applicationData.getRowCount()
    property int colCount: applicationData.getColCount()
    property int cellSize: (min(width, height) - imgMargin*2) / max(rowCount, colCount)//75
    property int labWidth: colCount * cellSize
    property int labHeight: rowCount * cellSize
    property int robotLeft: applicationData.robotColIdx * cellSize
    property int robotTop: applicationData.robotRowIdx * cellSize

    function isVertical(iIdx) {
        return applicationData.wallCol[iIdx].isOn
    }

    function isHorisontal(iIdx) {
        return applicationData.wallRow[iIdx].isOn
    }

    function isWork(iIdx) {
        return applicationData.work[iIdx].isOn
    }

    function getColor(iIdx) {
        //return (iNum % 3) == 0;
        return applicationData.getEnergyIdx() == iIdx ? "yellow" : "transparent"
    }

    Flickable {
        anchors.fill: parent
        leftMargin: imgMargin
        topMargin: imgMargin
        bottomMargin: imgMargin
        rightMargin: imgMargin
        contentWidth: image.width; contentHeight: image.height
        clip: true

        Rectangle {
            id: image
            //anchors.fill: parent
            width: labWidth
            height: labHeight
            color: "transparent"
            border.color : "black"

            Grid {
                id: labyrGrid
                columns: colCount
                rows: rowCount
                spacing: 0

                Repeater {
                    model: labyrGrid.columns * labyrGrid.rows
                    Rectangle {
                        //id: rectangle
                        //anchors.fill: parent
                        width: cellSize
                        height: cellSize
                        color: getColor(index)

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                applicationData.moveRobot(index)
                            }
                        }

                        Text {
                            text: index + 1
                            font: applicationData.getLogFont()
                        }
                        Shape {
                            anchors.fill: parent
                            //visible: false
                            visible: isVisibleHorisontal && isHorisontal(index)

                            ShapePath {
                                strokeWidth: 1
                                strokeColor: "black"
                                startX: 1
                                startY: cellSize// - 1
                                PathLine {
                                    x: cellSize
                                    y: cellSize// - 1
                                }
                            }
                        }
                        Shape {
                            anchors.fill: parent
                            visible: isVisibleVertical && isVertical(index)

                            ShapePath {
                                strokeWidth: 1
                                strokeColor: "black"
                                startX: cellSize
                                startY: 0
                                PathLine {
                                    x: cellSize
                                    y: cellSize// - 1
                                }
                            }
                        }
                        Shape {
                            anchors.fill: parent
                            visible: isVisibleWork && isWork(index)

                            ShapePath {
                                strokeWidth: 1
                                strokeColor: "black"
                                startX: 1
                                startY: cellSize / 4
                                PathLine {
                                    x: cellSize / 4
                                    y: cellSize / 2
                                }
                                PathMove {
                                    x: 1
                                    y: cellSize / 2
                                }
                                PathLine {
                                    x: cellSize / 4
                                    y: cellSize / 4
                                }
                            }
                        }
                    }
                }

                Shape {
                    visible: true

                    ShapePath {
                        strokeWidth: 1
                        strokeColor: "black"
                        startX: robotLeft + cellSize/4
                        startY: robotTop + cellSize/2
                        PathArc {
                            x: robotLeft + 3*cellSize/4;
                            y: robotTop + cellSize/2
                            radiusX: cellSize/4; radiusY: cellSize/4
                            useLargeArc: true
                        }
                        PathLine {
                            x: robotLeft + cellSize/4
                            y: robotTop + cellSize/2
                        }
                    }
                }
            }

        }
    }
}
