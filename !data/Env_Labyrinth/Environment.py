import typing

import model.emb as emb
import Labyrinth
import Robot


# ==============================
class State(emb.IDynamic):
    LabMas: typing.List[Labyrinth.State]
    Rob: Robot.State
    _step_mas: typing.List[emb.IDynamic]

    def __init__(self, i_robot: Robot.State, i_lab_mas: typing.List[Labyrinth.State]):
        self.LabMas = i_lab_mas
        self.Rob = i_robot

        self._step_mas = []
        self._step_mas.append(self.Rob)

    def do_step(self):
        list(map(
            lambda s: s.do_step(),
            self._step_mas
        ))

    def restart(self):
        for it in self.LabMas:
            it.restart()

        self.Rob.restart()
