import QtQuick.Controls 2.0
import QtQuick 2.11

Item {
    anchors.fill: parent

    property font textFont: applicationData.getLogFont()

    Page {
        anchors.fill: parent

        header: Page {
            Column {
                id: topComp
                anchors.fill: parent

                Text {
                   id: simpleText
                    text: "Labyrinth"
                }

                ComboBox {
                    width: parent.width
                    height: simpleText.height
                    id: listLabView
                    model: ListModel {
                        id: cbItems
                    }

                    delegate: ItemDelegate {
                        topPadding: 0
                        bottomPadding: 0
                        width: listLabView.width
                        contentItem: Text {
                            text: modelData
                            color: "Black"
                            font: listLabView.font
                            elide: Text.ElideRight
                            verticalAlignment: Text.AlignVCenter
                        }
                        highlighted: listLabView.highlightedIndex === index
                    }

                    contentItem: Text {
                        leftPadding: 0
                        rightPadding: listLabView.indicator.width + listLabView.spacing

                        text: listLabView.displayText
                        font: listLabView.font
                        color: "Black"//listLabView.pressed ? "#17a81a" : "#21be2b"
                        verticalAlignment: Text.AlignVCenter
                        elide: Text.ElideRight
                    }

                    onActivated: {
                        applicationData.setLabMasIdx(index);
                    }
                }

                Row {
                    width: parent.width
                    ToolButton {
                        text: "Restart"
                        onClicked: applicationData.restart()
                    }
                }

                Flickable {
                    width: parent.width
                    height: simpleText.height
                    clip: true
                    TextArea.flickable: TextArea {
                        font: applicationData.getLogFont()
                        readOnly: true
                        //selectByMouse: true
                        textFormat: TextEdit.PlainText
                        wrapMode: TextEdit.NoWrap
                        text: applicationData.statusText
                    }
                    ScrollBar.vertical: ScrollBar {}
                    ScrollBar.horizontal: ScrollBar {}
                }
            }

            Component.onCompleted: {
                var vCount = applicationData.getLabMasCount();
                for(var i=0; i<vCount; i++)
                {
                    var vItem = applicationData.getLabMasFile(i);
                    listLabView.model.append({text: vItem});
                }
                listLabView.currentIndex = 0
            }
        }

        FormLabyrinth {}
    }

    Component.onCompleted: {
        console.log("Completed CustomEnvironment Running!")
    }
}
