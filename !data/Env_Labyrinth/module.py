from dataclasses import dataclass
import glob
import os
import os.path as op

from PyQt5.QtGui import QFont
from PyQt5.QtQml import QQmlComponent, QQmlEngine, QQmlListProperty
from PyQt5.QtCore import QObject, pyqtSlot, pyqtProperty, pyqtSignal, QUrl, QCoreApplication  # , QTimer

import Robot
import Labyrinth
import Environment

if __name__ == '__main__':
    import sys

    cDir = op.dirname(__file__)
    vRootPath = op.join(cDir, '../../')
    vRootPath = op.abspath(vRootPath)
    sys.path.append(vRootPath)

    import model.emb as emb
else:
    import model.emb as emb


# ==============================
class PyBoolItem(QObject):
    onChanged = pyqtSignal()

    def __init__(self, i_is_on, parent=None):
        QObject.__init__(self, parent)
        self._isOn = i_is_on

    @pyqtProperty(bool, notify=onChanged)
    def isOn(self):
        return self._isOn

    def update(self, i_is_on):
        if self._isOn == i_is_on:
            return False
        self._isOn = i_is_on
        self.onChanged.emit()
        return True


# ==============================
class PyApplicationData(QObject):
    onRowsChanged = pyqtSignal()
    onColsChanged = pyqtSignal()
    onRobotChanged = pyqtSignal()
    onWorkChanged = pyqtSignal()
    onStatusChanged = pyqtSignal()
    _lab: Labyrinth.State
    _env_state: Environment.State
    _lab_idx: int

    def __init__(self, i_env_state: Environment.State):
        QObject.__init__(self)

        self._env_state = i_env_state
        self._select_lab(0)

        # self._timer = QTimer()
        # self._timer.timeout.connect(self._update_robot)
        # self._timer.start(1000)   # ms

    def get_lab_idx(self):
        return self._lab_idx

    def _select_lab(self, idx):
        self._lab_idx = idx
        self._lab = self._env_state.LabMas[self._lab_idx]

        # print(self._lab)

        self._col_count = self._lab.Size.Col
        self._row_count = self._lab.Size.Row

        vFullSize = self._row_count * self._col_count

        # self._wall_row = [PyBoolItem(i % 2 == 0) for i in range(vFullSize)]
        # self._wall_col = [PyBoolItem(i % 3 == 0) for i in range(vFullSize)]

        self._work = [PyBoolItem(it) for it in self._lab.CurrentDynamic.WorkMas]

        v_wall_row = [False for _ in range(vFullSize)]
        v_wall_col = [False for _ in range(vFullSize)]

        def con_col_to_idx(col, row):
            return (col - 1) + row * self._col_count

        for itCol, itRowRangeMas in self._lab.ColMap.items():
            for itRowRange in itRowRangeMas:
                for itRow in range(itRowRange.Min, itRowRange.Max):
                    idx = con_col_to_idx(itCol, itRow)
                    v_wall_col[idx] = True

        def con_raw_to_idx(col, row):
            return col + (row - 1) * self._col_count

        for itRow, itColRangeMas in self._lab.RowMap.items():
            for itColRange in itColRangeMas:
                for itCol in range(itColRange.Min, itColRange.Max):
                    idx = con_raw_to_idx(itCol, itRow)
                    v_wall_row[idx] = True

        self._wall_row = [PyBoolItem(i) for i in v_wall_row]
        self._wall_col = [PyBoolItem(i) for i in v_wall_col]

        self._energy_idx = self._lab.coord_to_idx(self._lab.EnergyPos)

        self._status_text = ""

        self.onRobotChanged.emit()
        self.onWorkChanged.emit()
        self.onRowsChanged.emit()
        self.onColsChanged.emit()

    def _update_robot(self):
        if self._lab.CurrentDynamic.RobotPos.Col < 4:
            self._lab.CurrentDynamic.RobotPos.Col = self._lab.CurrentDynamic.RobotPos.Col + 1
        else:
            self._lab.CurrentDynamic.RobotPos.Col = self._lab.CurrentDynamic.RobotPos.Col - 1

        self.onRobotChanged.emit()

    def update_work(self):
        for it, value in enumerate(self._lab.CurrentDynamic.WorkMas):
            self._work[it].update(value)

    @pyqtSlot(result=QFont)
    def getLogFont(self):
        return emb.get_log_font()

    @pyqtSlot(result=int)
    def getColCount(self):
        return self._col_count

    @pyqtSlot(result=int)
    def getRowCount(self):
        return self._row_count

    @pyqtProperty(str, notify=onStatusChanged)
    def statusText(self):
        return self._status_text

    @pyqtProperty(QQmlListProperty, notify=onRowsChanged)
    def wallRow(self):
        return QQmlListProperty(PyBoolItem, self, self._wall_row)

    @pyqtProperty(QQmlListProperty, notify=onColsChanged)
    def wallCol(self):
        return QQmlListProperty(PyBoolItem, self, self._wall_col)

    @pyqtProperty(QQmlListProperty, notify=onWorkChanged)
    def work(self):
        return QQmlListProperty(PyBoolItem, self, self._work)

    @pyqtProperty(int, notify=onRobotChanged)
    def robotColIdx(self):
        return self._lab.CurrentDynamic.RobotPos.Col

    @pyqtProperty(int, notify=onRobotChanged)
    def robotRowIdx(self):
        return self._lab.CurrentDynamic.RobotPos.Row

    @pyqtSlot(result=int)
    def getEnergyIdx(self):
        return self._energy_idx

    @pyqtSlot(result=int)
    def getLabMasCount(self):
        return len(self._env_state.LabMas)

    @pyqtSlot(int, result=str)
    def getLabMasFile(self, idx):
        return os.path.relpath(self._env_state.LabMas[idx].FileName, os.path.dirname(__file__))

    @pyqtSlot(int)
    def setLabMasIdx(self, idx):
        self._select_lab(idx)

    @pyqtSlot()
    def restart(self):
        self._env_state.restart()
        self.update_work()
        self.onRobotChanged.emit()

    @pyqtSlot(int)
    def moveRobot(self, i_pos):
        self._lab.robot_set_pos(i_pos)
        self.onRobotChanged.emit()


# ==============================
@dataclass
class CStateResult:
    Reward: bool
    Punishment: bool


# ==============================
class CModule(emb.IModule):
    _qml_data: PyApplicationData

    def __init__(self):
        self.Param = {
            emb.State.Input: {
                "Go": 4,  # North, East, South, West
                "DoWork": 1
            },
            emb.State.Inner: {"": 1},
            emb.State.Output: {
                "GCarrot": 1,
                "GStick": 1,
                "Hungry": 1,
                "HearWork": 1,
                "HearFood": 1,
                "Wall": 4  # North, East, South, West
            }
        }

        v_labyrinth_list = glob.glob(os.path.join(os.path.dirname(__file__), '*.labyr'))
        print('\r\n'.join(v_labyrinth_list))
        self._LabyrinthMas = [Labyrinth.State(op.join(op.dirname(__file__), it)) for it in v_labyrinth_list]

        self.State = Environment.State(
            Robot.State(Robot.CEnergy(i_max_energy=500, i_low_energy=40)),
            self._LabyrinthMas
        )

        if QCoreApplication.instance():
            self._QmlEngine = QQmlEngine()
            self._qml_data = PyApplicationData(self.State)
            self._QmlEngine.rootContext().setContextProperty(
                "applicationData", self._qml_data)

            v_dir = op.dirname(__file__)
            self._QmlEngine.addImportPath(v_dir)
            self._QmlEngine.setBaseUrl(QUrl("file:///" + v_dir+"/"))

            self._component = QQmlComponent(self._QmlEngine)
            self._component.loadUrl(QUrl("module.qml"))
            if self._component.status() != QQmlComponent.Ready:
                self._component = None

    def get_param(self):
        return self.Param

    def get_component(self):
        return self._component

    def step(self, i_tick_idx, i_input):
        # v_in = [i_input[it][0] for it in self.Param[emb.State.Input]]
        #
        # v_inner = self.State.is_start(i_tick_idx, v_in)
        # v_output = self.State.get_current()
        #

        v_current_state = CStateResult(False, False)

        v_do_work = (i_input["DoWork"][0] == 1)
        v_in_go = i_input["Go"]
        v_sum_go = sum(v_in_go)
        v_do_step = (v_sum_go == 1)

        vl_current_lab = self.State.LabMas[self._qml_data.get_lab_idx()]

        if v_do_work:
            if vl_current_lab.robot_do_work():
                self._qml_data.update_work()
                v_current_state.Reward = True
            else:
                v_current_state.Punishment = True

        if v_sum_go > 1:
            v_current_state.Punishment = True

        self.State.do_step()

        if v_do_step:
            # self.State.Lab.set_robot_pos(i_tick_idx % self.State.Lab.get_size_count())
            if vl_current_lab.robot_do_step(v_in_go):
                self._qml_data.onRobotChanged.emit()
            else:
                v_current_state.Punishment = True

        v_lab_state = vl_current_lab.get_state()

        v_robot_state = self.State.Rob.set_input(i_tick_idx, v_lab_state.HearEnergy, v_do_work, v_do_step)

        v_out = {it: [0] * count for it, count in self.Param[emb.State.Output].items()}

        v_out['GCarrot'][0] = 1 if v_current_state.Reward else 0
        v_out['GStick'][0] = 1 if v_current_state.Punishment else 0
        v_out['Hungry'][0] = 1 if v_robot_state.Hungry else 0
        v_out['HearWork'][0] = 1 if v_lab_state.HearWork else 0
        v_out['HearFood'][0] = 1 if v_lab_state.HearEnergy else 0
        v_out['Wall'] = [1 if it else 0 for it in vl_current_lab.get_robot_state()]

        v_state = (v_current_state, v_lab_state, v_robot_state)
        self._qml_data._status_text = 'Tick: {}'.format(i_tick_idx)
        self._qml_data.onStatusChanged.emit()

        v_result = (
            v_state,
            v_out
        )

        return v_result


# ==============================
def test(i_step=0):
    from PyQt5.QtGui import QGuiApplication
    from PyQt5.QtQml import QQmlApplicationEngine

    app = QGuiApplication(sys.argv)

    v_module = CModule()

    for i in range(10):
        if i == 5:
            v_in = {"Search": [1], "Rest": [0]}
        else:
            v_in = {"Search": [0], "Rest": [0]}

        v_step_result = v_module.step(i_step + i, v_in)

        print('In: {}\r\nOut: {}\r\nEn: {}'.format(
            v_in,
            v_step_result,
            v_module.State.Rob.Energy.Value
        ))

    cmp = v_module.get_component()

    engine = QQmlApplicationEngine()
    engine.load("module_test.qml")
    engine.rootObjects()[0].setCmpt(cmp)

    engine.quit.connect(app.quit)
    app.exec_()


# ==============================
if __name__ == '__main__':
    import sys
    import contextlib

    def do_with_file(fun):
        with open(op.join(op.dirname(__file__), 'std_out.txt'), 'w') as fOut:
            with contextlib.redirect_stdout(fOut):
                with open(op.join(op.dirname(__file__), 'std_err.txt'), 'w') as fErr:
                    with contextlib.redirect_stderr(fErr):
                        fun()

    is_with_file = False
    if is_with_file:
        do_with_file(test)
    else:
        test()
