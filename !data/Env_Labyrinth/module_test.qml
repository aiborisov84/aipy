import QtQuick 2.11
import QtQuick.Controls 2.0

ApplicationWindow {
    property Item envItem: null
    visible: true
    width: 500
    height: 500
    title: qsTr("test QML")
    id: main
       
    function setCmpt(iComponent)
	{
		console.log("Completed Component Insert!")
	    envItem = iComponent.createObject(main, {"x": 0, "y": 0});
	}
}