import os.path as op
import model.layercore.state
import model.layercore.cell
import model.layercore.analyse
import model.net

import logging
import logging.handlers
import queue

import glob
import os

G_Cases = glob.glob(os.path.join(os.path.dirname(__file__), '!data/Env*/*.work_net'))
G_Cases.sort()
print("\n".join(G_Cases))


# ==============================
class CTest:
    def __init__(self, i_env_file):
        self.File = op.abspath(i_env_file)
        self.Net = model.net.CNet.create_from_file(self.File)
        self.q = queue.Queue(100)  # no limit on size (-1)
        self._qh = logging.handlers.QueueHandler(self.q)
        v_root = logging.getLogger()
        v_root.setLevel(logging.DEBUG)
        v_root.addHandler(self._qh)

    def __del__(self):
        v_root = logging.getLogger()
        v_root.removeHandler(self._qh)
    
    def do(self):
        v_result = []
        try:
            self.Net.do_tick()
        except Exception as e:
            logging.exception('---------------------- exception')
            logging.error('---------------------- ', exc_info=e)
            logging.debug('----------------------')
        
        while not self.q.empty():
            record = self.q.get()
            if record is None:
                break
            v_result.append(record.getMessage())
        
        return v_result

    def get_components(self):
        v_env_component_mas = []
        v_layer_component_mas = []
        try:
            v_env_component_mas = self.Net.get_env_components()
        except Exception as e:
            logging.exception('---------------------- exception env')
            logging.error('---------------------- ', exc_info=e)
            logging.debug('----------------------')

        try:
            v_layer_component_mas = self.Net.get_layer_components()
        except Exception as e:
            logging.exception('---------------------- exception layer')
            logging.error('---------------------- ', exc_info=e)
            logging.debug('----------------------')

        return v_env_component_mas, v_layer_component_mas
        

# ==============================
if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, format='%(message)s')
    vFile = op.abspath('!data/Env1/Test.work_net')

    vPrefix = ''.join(['=']*50)

    def header(i_caption):
        print(vPrefix+' Test '+i_caption)
    
    header('Analyse')
    model.layercore.analyse.test()
    
    header('State')
    model.layercore.state.test()
        
    header('Net')
    model.net.test(vFile)
    
    vTest = CTest(G_Cases[1])
    for _ in range(120):
        vState = vTest.do()
        
    header('End')
