import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.2
import QtQml 2.11
import QtQuick.Controls 1.4 as C
 
ApplicationWindow {
    id: fmMain
    visible: true
    width: 1200
    height: 800//1920
    title: qsTr("ai Net")
    property font textFont: pyconnect.getLogFont()
    property Item envItem: null
    property Item layerItem: null
    
    ListModel {
        id: viewList
    }

    Component {
        id: viewDelegateTab

        Page {
            title: index+1
            Flickable {
                id: flick
                property int lastY: 0
                property bool userChage: false
                anchors.fill: parent
                clip: true
                TextArea.flickable: TextArea {
                    id: messageArea
                    font: textFont
                    readOnly: true
                    //selectByMouse: true
                    textFormat: TextEdit.PlainText
                    wrapMode: TextEdit.NoWrap
                    text: content
                }
                ScrollBar.vertical: ScrollBar {}
                ScrollBar.horizontal: ScrollBar {}
                onContentYChanged: {
                    if(!userChage)
                        if(contentY!=lastY)
                            contentY=lastY
                }
                onMovementStarted: {
                    userChage = true
                }
                onMovementEnded: {
                    userChage = false
                    lastY = contentY
                }
            }
        }
    }

    Page {
        anchors.fill: parent

        StackLayout {
            id: swipeViewType
            anchors.fill: parent

            Page {
                id: viewLogTab

                StackLayout {
                    id: swipeView
                    anchors.fill: parent
                    currentIndex: tabBar.currentIndex

                    Repeater {
                        model: viewList
                        delegate: viewDelegateTab
                    }
                }

                footer: TabBar {
                    id: tabBar

                    Repeater {
                        model: viewList
                        TabButton {
                            text: item_title
                        }
                    }
                }
            }

            C.SplitView {
                id: splitEnvLayer
                orientation: Qt.Horizontal

                Page {
                    width: parent.width*0.3
                    height: parent.height
                    id: viewEnvTab
                }

                Page {
                    width: parent.width*0.7
                    height: parent.height
                    id: viewLayerTab
                }
            }
        }

        footer: ToolBar {
            Row {
                anchors.fill: parent
                layoutDirection: Qt.RightToLeft

                TabButton {
                    id: btEnvLayerTab
                    text: "Env[e] + Layer[l]"
                    //width: parent.width*0.2
                    onClicked: swipeViewType.currentIndex = 1
                }

                TabButton {
                    id: btHistoryTab
                    text: "LogHistory[1-5]"
                    //width: parent.width*0.2
                    onClicked: swipeViewType.currentIndex = 0
                }
            }
        }
    }

    footer: ToolBar {
        id: tb1

        Row {
            anchors.fill: parent
            layoutDirection: Qt.RightToLeft

            SpinBox {
                id: stepCount
                value: 1
                from: 1
            }

            ToolButton {
                text: "step[9]"
                onClicked: step()
            }

            ToolButton {
                text: "restart[0]"
                onClicked: restart()
            }

            ToolButton {
                text: "exit"
                onClicked: {
                    Qt.quit()
                }
            }

            ToolButton {
                text: "select[s]"
                onClicked: selectModel()
            }
        }
    }
    
    function step() {
        for(var i=0; i<viewList.count-1; i++)
        {
            viewList.setProperty(i, "content", 
                viewList.get(i+1).content
            )
        }
        viewList.setProperty(viewList.count-1, "content", pyconnect.step(stepCount.value))
    }

    function restartIdx(idx) {
        pyconnect.restart(idx)
        for(var i=0; i<viewList.count; i++)
        {
            viewList.setProperty(i, "content", viewList.get(i).item_title)
        }
    }
    
    function restart() {
        restartIdx(-1)
    }

    function setEnv(iComponent)
    {
        if(envItem)
        {
            envItem.destroy()
            envItem = null
        }

        if(iComponent)
        {
            console.log("Ready Environment Insert!")
            envItem = iComponent.createObject(viewEnvTab, {"x": 0, "y": 0});
            viewEnvTab.visible = true
        }
        else
        {
            viewEnvTab.visible = false
        }
        selectImportant()
    }

    function setLayer(iComponent)
    {
        if(layerItem)
        {
            layerItem.destroy()
            layerItem = null
        }

        if(iComponent)
        {
            console.log("Ready Layer Insert!")
            layerItem = iComponent.createObject(viewLayerTab, {"x": 0, "y": 0});
            viewLayerTab.visible = true
        }
        else
        {
            viewLayerTab.visible = false
        }
        selectImportant()
        tabBar.currentIndex = viewList.count-1
    }

    function selectModel()
    {
        dlgSelectModel.visible = true
    }

    Component.onCompleted: {
        if(!pyconnect.isAndroid())
        {
            fmMain.height = Screen.desktopAvailableHeight * 0.8;
            fmMain.width = Screen.desktopAvailableWidth * 0.8;
            fmMain.y = Screen.desktopAvailableHeight * 0.1;
            fmMain.x = Screen.desktopAvailableWidth * 0.1;
        }

        for(var i=5; i>0; i--)
        {
            viewList.append(
            {
                "content": (i).toString(),
                "item_title": (i).toString()
            })
        }
    }

    function selectHistory(iIndex)
    {
        tabBar.currentIndex = viewList.count-iIndex
        btHistoryTab.clicked()
        btHistoryTab.checked = true
    }

    function selectEnvLayer()
    {
        if(!btEnvLayerTab.visible)
            return false
        btEnvLayerTab.clicked()
        btEnvLayerTab.checked = true
        return true
    }

    function selectImportant()
    {
        if(selectEnvLayer())
            return
        selectHistory(1)
    }

    Shortcut {
        sequence: "1"
        onActivated: selectHistory(1)
    }
    Shortcut {
        sequence: "2"
        onActivated: selectHistory(2)
    }
    Shortcut {
        sequence: "3"
        onActivated: selectHistory(3)
    }
    Shortcut {
        sequence: "4"
        onActivated: selectHistory(4)
    }
    Shortcut {
        sequence: "5"
        onActivated: selectHistory(5)
    }
    Shortcut {
        sequence: "e"
        onActivated: selectEnvLayer()
    }
    Shortcut {
        sequence: "l"
        onActivated: selectEnvLayer()
    }
    Shortcut {
        sequence: "9"
        onActivated: step()
    }
    Shortcut {
        sequence: "0"
        onActivated: restart()
    }
    Shortcut {
        sequence: "s"
        onActivated: selectModel()
    }
    
    Dialog {
        id: dlgSelectModel
        visible: false
        title: "Choose model"
        standardButtons: StandardButton.Ok
    
        onAccepted: {
            restartIdx(listView.currentIndex)
        }

        contentItem: Rectangle {
            width: fmMain.width * 0.8
            height: fmMain.height * 0.8
            Page {
                anchors.fill: parent
                ListView {
                    anchors.fill: parent
                    id: listView
                    model: ListModel {
                        id: cbItems
                    }
                    clip: true
                    highlight: Rectangle {
                        color: "lightsteelblue"
                        radius: 5
                    }
                    highlightMoveDuration: 100
                    highlightMoveVelocity: -1
                    delegate: ItemDelegate {
                        text: "№" + index+" "+modelName
                        //highlighted: ListView.isCurrentItem
                        onClicked: {
                            listView.currentIndex = index
                            dlgSelectModel.accept()
                        }
                    }

                    ScrollBar.vertical: ScrollBar {
                        active: true
                    }
                    ScrollBar.horizontal: ScrollBar {
                        active: true
                    }
                }
                footer: ToolBar {
                    Row {
                        anchors.fill: parent
                        layoutDirection: Qt.RightToLeft
                        ToolButton {
                            text: "Ok"
                            onClicked: dlgSelectModel.accept()
                        }
                    }
                }
            }
        }

        Component.onCompleted: {
            if(!pyconnect.isAndroid())
            {
                dlgSelectModel.width = fmMain.width * 0.8
                dlgSelectModel.height = fmMain.height * 0.8
            }

            var vCount = pyconnect.modelCount();
            for(var i=0; i<vCount; i++) 
            {
                var vItem = pyconnect.modelStr(i);
                listView.model.append({"modelName": vItem});
            }   
            listView.currentIndex = pyconnect.modelIdx()
        }
    }
}
