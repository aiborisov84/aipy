from PyQt5.QtGui import QGuiApplication, QFont
from PyQt5.QtQml import QQmlApplicationEngine
from PyQt5.QtCore import QObject, pyqtSlot

import test
import model.emb
import typing


# ==============================
class PyConnect(QObject):
    _Test: typing.Optional[test.CTest]

    def __init__(self):
        QObject.__init__(self)

        self._Root = None
        self._Test = None
        self._ModelIdx = len(test.G_Cases)-1
        self._restart()

    def set_root(self, i_root):
        self._Root = i_root
        self._refresh_env_component()

    def _refresh_env_component(self):
        if self._Root is None:
            return

        if self._Test is not None:
            v_env_com_mas, v_layer_com_mas = self._Test.get_components()

            for it_comp in v_env_com_mas:
                self._Root.setEnv(it_comp)
                break
            else:
                self._Root.setEnv(None)

            for it_comp in v_layer_com_mas:
                self._Root.setLayer(it_comp)
                break
            else:
                self._Root.setLayer(None)

    def _restart(self):
        self._Test = test.CTest(test.G_Cases[self._ModelIdx])
        self._refresh_env_component()

    @pyqtSlot(int, result=str)
    def step(self, i_step_count):
        v_state = []

        for _ in range(i_step_count):
            v_state = self._Test.do()

        return '\r\n'.join(v_state)

    @pyqtSlot(result=int)
    def modelCount(self):
        return len(test.G_Cases)

    @pyqtSlot(int, result=str)
    def modelStr(self, i_idx):
        return os.path.relpath(test.G_Cases[i_idx])

    @pyqtSlot(result=int)
    def modelIdx(self):
        return self._ModelIdx

    @pyqtSlot(int)
    def restart(self, i_idx):
        if i_idx >= 0:
            self._ModelIdx = i_idx
        self._restart()

    @pyqtSlot(result=QFont)
    def getMainFont(self):
        return model.emb.get_appl_font()
        
    @pyqtSlot(result=QFont)
    def getLogFont(self):
        return model.emb.get_log_font()

    @pyqtSlot(result=bool)
    def isAndroid(self):
        return model.emb.GIsDroid


# ==============================
if __name__ == "__main__":
    import sys
    import os

    app = QGuiApplication(sys.argv)
    py_connect = PyConnect()
    app.setFont(py_connect.getMainFont())
    engine = QQmlApplicationEngine()

    engine.rootContext().setContextProperty("pyconnect", py_connect)
    engine.load(os.path.join(os.path.dirname(__file__), "Appl1.qml"))

    py_connect.set_root(engine.rootObjects()[0])

    engine.quit.connect(app.quit)
    sys.exit(app.exec_())
