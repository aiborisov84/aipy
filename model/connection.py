from .item import CItem
from . import xml


# ==============================
class LineAddressT:
    def __init__(self, i_name, i_start_idx):
        self.Name = i_name
        self.StartIdx = i_start_idx
        

# ==============================
class LinkT:
    def __init__(self, i_from, i_to, i_count):
        self.From = i_from
        self.To = i_to
        self.Count = i_count
        

# ==============================
class CConnection(CItem):
    def __init__(self, i_params):
        super().__init__(i_params)
        self.LinkMas = i_params['LinkMas']
        self.From = i_params['From']
        self.To = i_params['To']
    
    # ==============================
    @classmethod
    def create_from_xml(cls, i_xml_item, i_block_getter):
        v_params = super().param_from_xml(i_xml_item)
        v_version = i_xml_item.attributes['Version'].value
        
        c_current_version = '1.0'
        if v_version != c_current_version:
            raise Exception('Bad version')
        
        v_data = i_xml_item.getElementsByTagName('Data')[0]
        v_from_id = xml.get_int(v_data, 'FromID')
        v_to_id = xml.get_int(v_data, 'ToID')
        
        v_params['From'] = i_block_getter(v_from_id)
        v_params['To'] = i_block_getter(v_to_id)
        
        v_link_list = v_data.getElementsByTagName('Link')
        
        v_link_mas = []
        for itLink in v_link_list:
            v_new_link = LinkT(
                i_from=LineAddressT(
                    i_name=xml.get_optional(itLink, 'FromLineName', ''),
                    i_start_idx=xml.get_int(itLink, 'FromLineIdx')
                ),
                i_to=LineAddressT(
                    i_name=xml.get_optional(itLink, 'ToLineName', ''),
                    i_start_idx=xml.get_int(itLink, 'ToLineIdx')
                ),
                i_count=xml.get_int(itLink, 'LineCount')
            )
            v_link_mas.append(v_new_link)
        
        v_params['LinkMas'] = v_link_mas
          
        return cls(v_params)
    
    # ==============================    
    def save(self, i_parent_node):
        pass
        
    # ==============================
    def do_transmit(self):
        for itLink in self.LinkMas:
            v_data = self.From.get_output(itLink.From, itLink.Count)
            self.To.set_input(itLink.To, v_data)
