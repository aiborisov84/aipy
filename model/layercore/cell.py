from __future__ import annotations
import typing
from dataclasses import dataclass
import unittest

from . import transfer
from . import group
from .. import emb, xml
from .state import CRangeValue


# ==============================
@dataclass
class CCellParam:
    CompetitionTime: int
    RelaxTime: int
    StrongRange: int


# ==============================
class CCell:
    # время соревнования внутри слоя (CompetitionTime)
    _CompetitionTime: CRangeValue
    # время отдыха (RelaxTime)
    _RelaxTime: CRangeValue
    # количество использований ячейки TODO: спорный параметр
    # используется только для выбора последовательности первого использования ячейки
    Strong: CRangeValue
    Active: bool

    def __init__(self, i_param: CCellParam):
        self._CompetitionTime = CRangeValue(i_param.CompetitionTime)
        self._RelaxTime = CRangeValue(i_param.RelaxTime)
        self.Strong = CRangeValue(i_param.StrongRange)
        self.Active = False

    def __repr__(self):
        v_state = '[{} T:{} R:{} S:{}]'.format(
            '#' if self.Active else ' ',
            self._CompetitionTime,
            self._RelaxTime,
            self.Strong
        )

        return v_state

    def load(self, i_state):
        if i_state is None:
            return

        self._CompetitionTime.Value = i_state[0]
        self._RelaxTime.Value = i_state[1]
        self.Strong.Value = i_state[2]
        self.Active = i_state[3]

    def work__set(self, i_is_active):
        self.Active = i_is_active
        self._CompetitionTime.set_max()
        self.Strong.inc()

    def work__transfer(self):
        self.Active = False

    def work__is_run(self):
        return self._CompetitionTime.is_set() or self._RelaxTime.is_set()

    def work__is_current(self):
        return self._CompetitionTime.is_max()

    def work__is_ready(self):
        return self._CompetitionTime.is_min_next()

    def strong__inc(self):
        self.Strong.inc()

    def time__step(self):
        if self._CompetitionTime.is_min_next():
            self._RelaxTime.set_max()

        self._CompetitionTime.dec()
        self._RelaxTime.dec()

        if not self._CompetitionTime.is_set() and not self._RelaxTime.is_set():
            self.Active = False

    def is_active(self):
        return self.Active

    def competition__get_time(self):
        return self._CompetitionTime.Value


# ==============================
class CAnalyseCell(CCell):
    _TransferList: typing.List[transfer.CTransfer]
    _Competition: typing.Optional[int]
    _Caption: str

    def __init__(self, i_param: CCellParam):
        super().__init__(i_param)
        self._TransferList = []
        self._Competition = None
        self._Caption = ''

    def __repr__(self):
        v_competition_str = ''
        if self._Competition is not None:
            v_competition_str = f' Comp({self._Competition})'

        c_indent = emb.GIndent

        v_result = [c_indent + super().__repr__() + v_competition_str]

        for it in self._TransferList:
            v_result.append(c_indent + str(it))

        return '\r\n'.join(v_result)

    def step(self):
        self.time__step()

        for it in self._TransferList:
            it.step()

    def reward(self, i_reward: typing.Optional[bool]):
        if i_reward is None:
            return

        if not i_reward:
            return

        for it in self._TransferList:
            it.reward()

    def stick(self, i_stick: typing.Optional[bool]):
        if i_stick is None:
            return

        if not i_stick:
            return

        for it in self._TransferList:
            it.stick()

    def get_mem_active_range(self):
        if not self._TransferList:
            return 0

        return max(it.get_active_range() for it in self._TransferList)

    def add_transfer(self, i_transfer: transfer.CTransfer) -> bool:
        # если ячейка задавлена - необходимо выбрать другую для активации
        if self._Competition is not None:
            if self._Competition <= 0:
                return False

        for it in self._TransferList:
            if it.is_same(i_transfer):
                return False

        self._TransferList.append(i_transfer)
        return True

    def competition_run(
            self,
            i_input_cell_group: group.CCellGroup,
            i_inner_cell_group: group.CCellGroup) -> typing.List[transfer.CTransfer]:

        v_is_work = False
        v_result = []
        v_competition_sum = 0

        for it in self._TransferList:
            # Проверка применимости
            if not it.is_catch_state(i_input_cell_group, i_inner_cell_group):
                continue

            v_competition_voice = it.get_competition_voice()
            # todo: condition transfer with check double link in creation
            if v_competition_voice > 0:
                v_result.append(it)

            v_competition_sum += v_competition_voice

            v_is_work = True

        if v_is_work:
            self._Competition = v_competition_sum

        return v_result

    def competition_reset(self):
        self._Competition = None

        for it in self._TransferList:
            it.work_prepare()

    def competition_result(self) -> bool:
        if self._Competition is None:
            return False

        if self._Competition <= 0:
            return False
        else:
            return True

    def transfer_optimization(self):
        self._TransferList = [it for it in self._TransferList if not it.is_useless()]

    def load(self, i_xml_state):
        if i_xml_state is None:
            return

        self._Caption = xml.get_optional(i_xml_state, "Caption", "")

        # self.Level.Value = i_state[0]
        # self.Strong.Value = i_state[1]
        # self.Active = i_state[2]

        super().load(None)

    def get_caption(self):
        if self._Caption:
            return '"{}"'.format(self._Caption)
        else:
            return ''

    def set_transfer_caption(
            self,
            i_input_cell_group: group.CCellGroup,
            i_inner_cell_group: group.CCellGroup
    ):
        for it in self._TransferList:
            it.set_caption(i_input_cell_group, i_inner_cell_group)


# ==============================
class TestCellMethods(unittest.TestCase):

    def test_upper(self):
        v_param = CCellParam(1,15,8)
        v = CCell(v_param)
        v.work__set(True)
        v.strong__inc()
        print(v)


# ==============================
if __name__ == '__main__':
    unittest.main()
