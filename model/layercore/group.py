from __future__ import annotations
import typing

from . import cell
from . import transfer
from .. import xml
from .state import CState, CRangeValue


# ==============================
class CCellChain:
    _CellIdxMas: typing.List[int]

    def __init__(self, i_max_series_length):
        self.MaxSeriesLength = i_max_series_length
        self._CellIdxMas = []

    def load(self, i_state):
        if i_state is None:
            self._CellIdxMas = []
        else:
            self._CellIdxMas = i_state

    def get_last_cell_idx(self):
        if len(self._CellIdxMas) == 0:
            return None
        else:
            return self._CellIdxMas[-1]

    def reset(self):
        self._CellIdxMas = []

    def add_cell_idx(self, i_cell_idx):
        if len(self._CellIdxMas) < self.MaxSeriesLength:
            self._CellIdxMas.append(i_cell_idx)

    def is_can_learn(self):
        return len(self._CellIdxMas) < self.MaxSeriesLength

    def is_none(self):
        return len(self._CellIdxMas) == 0

    def is_contains(self, i_cell_idx):
        return i_cell_idx in self._CellIdxMas


# ==============================
class CCellGroup:
    CellMas: typing.Mapping[int, cell.CAnalyseCell]
    CurrentChain: CCellChain
    NoneLevel: typing.Optional[CRangeValue]
    _Caption: str
    _TransferParam: transfer.CTransferParam
    _InitCurrentCellIdx: typing.Optional[int]

    def __init__(
            self,
            i_caption: str,
            i_max_chain_length: int,
            i_none_action: bool,
            i_cell_count: int,
            i_cell_param: cell.CCellParam,
            i_transfer_param: transfer.CTransferParam):

        self._TransferParam = i_transfer_param
        self._Caption = i_caption
        self.CellMas = {
            it: cell.CAnalyseCell(i_cell_param)
            for it in range(i_cell_count)
        }

        self._InitCurrentCellIdx = None
        self.CurrentChain = CCellChain(
            i_max_series_length=i_max_chain_length
        )

        if i_none_action:
            self.NoneLevel = CRangeValue(i_cell_param.CompetitionTime + i_cell_param.RelaxTime)
        else:
            self.NoneLevel = None

    # ==============================
    def _get_active_cell_idx(self) -> typing.Optional[int]:
        v_result_cell_idx = self.CurrentChain.get_last_cell_idx()
        if v_result_cell_idx is None:
            v_result_cell_idx = self._InitCurrentCellIdx

        return v_result_cell_idx

    # ==============================
    def _get_min_strong_cell_idx(self) -> typing.Optional[int]:
        """Получение индекса самой слабой ячейки"""
        v_result = [None]
        for it, itCell in self.CellMas.items():
            v_strong = itCell.Strong.Value
            if v_result[0] is None or v_result[0] > v_strong:
                v_result = [v_strong, it]

        return v_result[1] if v_result[0] is not None else None

    # ==============================
    def _get_rest_active(self) -> transfer.CActiveCellsState:
        v_result = {}
        v_result_range = 1

        for it_cell_idx, it_cell in self.CellMas.items():
            if not it_cell.work__is_ready():
                continue

            if not it_cell.is_active():
                continue

            v_result[it_cell_idx] = it_cell.competition__get_time()
            v_result_range = max(
                v_result_range,
                it_cell.get_mem_active_range() + 1
            )

        return transfer.CActiveCellsState(v_result, v_result_range)

    # ==============================
    def _get_chain_active(self) -> transfer.CActiveCellsState:
        """Получает массив индексов и уровней текущих активных ячеек и
        глубину предшествующей истории
        """
        v_result = {}
        v_result_range = 1
        c_last_cell_idx = self._get_active_cell_idx()

        def try_add_cell(i_cell_idx: int):
            v_cell = self.CellMas[i_cell_idx]

            if not v_cell.work__is_run():
                return
            if not v_cell.is_active():
                return
            if v_cell.work__is_current():
                return

            nonlocal v_result
            nonlocal v_result_range

            v_result[i_cell_idx] = v_cell.competition__get_time()
            v_result_range = max(
                v_result_range,
                v_cell.get_mem_active_range() + 1
            )

        # Добавляем в опору все ячейки максимального уровня (сработавшие на предыдущем шаге)
        # Необходимо заменить это на добавление ячеек, сработавших последними в своих цепочках
        # (отвязаться от требования на предыдущий шаг)
        for it_cell_idx in self.CellMas:
            if c_last_cell_idx == it_cell_idx:
                continue

            try_add_cell(it_cell_idx)

        # последней в опору добавляем текущую ячейку
        if c_last_cell_idx is not None:
            try_add_cell(c_last_cell_idx)

        return transfer.CActiveCellsState(v_result, v_result_range)

    # ==============================
    def set_state(self, i_input_state: CState):
        assert(i_input_state.get_size() == len(self.CellMas))

        for it_cell_idx, it_cell in self.CellMas.items():
            if i_input_state.is_set_item(it_cell_idx):
                it_cell.work__set(True)

    # ==============================
    def step(self):
        if self.NoneLevel is not None:
            self.NoneLevel.inc()

        for itCell in self.CellMas.values():
            itCell.step()

            if itCell.is_active():
                if self.NoneLevel is not None:
                    self.NoneLevel.reset()

        if self._InitCurrentCellIdx is not None:
            vl_active_cell = self.CellMas[self._InitCurrentCellIdx]
            if not vl_active_cell.work__is_run():
                self._InitCurrentCellIdx = None

        c_last_cell = self.CurrentChain.get_last_cell_idx()
        if c_last_cell is not None:
            vl_active_cell = self.CellMas[c_last_cell]
            if not vl_active_cell.work__is_run():
                self.CurrentChain.reset()

    # ==============================
    def __repr__(self):
        v_cell_mas = []

        if self.NoneLevel is not None:
            v_cell_mas.append('NoneLevel: {}'.format(self.NoneLevel.Value))

        for it_cell_idx, it_cell in self.CellMas.items():
            v_cell_mas.append('{}{}: {}'.format(
                it_cell_idx,
                ('#' if it_cell.work__is_current() else ' ') +
                ('+' if it_cell_idx == self.CurrentChain.get_last_cell_idx() else ' ') +
                ('*' if it_cell_idx == self._InitCurrentCellIdx else ' '),
                it_cell.get_caption()
            ))
            v_cell_mas.append(str(it_cell))

        c_indent = ''  # emb.GIndent
        v_cells = (
            '\r\n'.join([
                self._Caption + ' [',
                *[c_indent + it for it in v_cell_mas],
                ']',
                'InnerActive:' + str(self._get_chain_active()),
                'OutRestActive:' + str(self._get_rest_active())
            ])
        )

        return v_cells

    # ==============================
    def _dispatch_input_chains_work(self, i_input_cell_group: CCellGroup):

        for it_cell in self.CellMas.values():
            it_cell.competition_reset()

        for it_cell_idx, it_cell in self.CellMas.items():
            if it_cell.work__is_run():
                continue

            if self.CurrentChain.is_contains(it_cell_idx):
                continue

            # Перенос активности ячейки с входных и внутренних в соревнование текущей ячейки
            v_work_transfer_list = it_cell.competition_run(i_input_cell_group, self)

            for it_transfer in v_work_transfer_list:
                it_transfer.work_transfer(i_input_cell_group, self)

        v_max_strong = None

        for it_cell_idx, it_cell in self.CellMas.items():

            if not it_cell.competition_result():
                # todo: may be inhibition work (try copy link to other cell)
                continue

            it_cell.work__set(True)

            v_strong = it_cell.Strong.Value
            if v_max_strong is None or v_max_strong[0] < v_strong:
                v_max_strong = [v_strong, it_cell_idx]

            if self.NoneLevel is not None:
                self.NoneLevel.reset()

        # Самая сильная активированная ячейка
        # становится текущей
        if v_max_strong is not None:
            self._InitCurrentCellIdx = v_max_strong[1]

    # ==============================
    def _dispatch_input_chain_learn(self, i_input_cell_group: CCellGroup):
        if not self.CurrentChain.is_can_learn():
            return

        v_input_active = i_input_cell_group._get_rest_active()
        if v_input_active.is_none():
            if self.NoneLevel is None:
                return
            if not self.NoneLevel.is_max():
                return

        v_inner_active = self._get_chain_active()
        if self.CurrentChain.MaxSeriesLength < v_inner_active.Range:
            return

        # Если входная маска еще не вся разобрана, то
        # на эту маску устанавливается обработка

        v_transfer = transfer.CTransfer(
            v_input_active,
            v_inner_active,
            self._TransferParam
        )

        self._use_new_cell(i_input_cell_group, v_transfer)

    # ==============================
    def _use_new_cell(self, i_input_cell_group, i_transfer):
        # ищется слабая ячейка
        # и устанавливается обработка

        for it_idx, it_cell in sorted(self.CellMas.items(), key=lambda item: item[1].Strong.Value):
            if not it_cell.add_transfer(i_transfer):
                continue

            i_transfer.work_transfer(i_input_cell_group, self)
            it_cell.work__set(True)

            self.CurrentChain.add_cell_idx(it_idx)
            break

    # ==============================
    def _dispatch_inner_chain_learn(self):
        """Если вход разобран, и формируется цепочка, и сработал старт других цепочек,
        То прописываем эти старты в формирующуюся цепочку"""

        if self.CurrentChain.is_none():
            return

        v_last_chain_cell_idx = self.CurrentChain.get_last_cell_idx()

        vl_last_chain_cell = self.CellMas[v_last_chain_cell_idx]

        if vl_last_chain_cell.work__is_current():
            return

        v_input_active = transfer.CActiveCellsState({}, 1)  # чистый вход
        v_current_active = transfer.CActiveCellsState(
            {v_last_chain_cell_idx: vl_last_chain_cell.competition__get_time()},
            vl_last_chain_cell.get_mem_active_range() + 1
        )

        if self.CurrentChain.MaxSeriesLength < v_current_active.Range:
            return

        v_transfer = transfer.CTransfer(
            v_input_active,
            v_current_active,
            self._TransferParam
        )

        v_is_linked = False
        for it_cell_idx, it_cell in self.CellMas.items():
            if self.CurrentChain.is_contains(it_cell_idx):
                continue
            # Первая ячейка в цепочке
            if it_cell.get_mem_active_range() != 1:
                continue
            # Активная в текущий момент
            if not it_cell.is_active():
                continue
            if not it_cell.work__is_current():
                continue

            it_cell.add_transfer(v_transfer)

            v_transfer.work_transfer(None, self)

            v_is_linked = True

        if not v_is_linked:
            return

        # !!! Добавить новую ячейку в цепь
        self._use_new_cell(None, v_transfer)

    # ==============================
    def _dispatch_input_rest(self, i_input_cell_group: CCellGroup):
        # оптимизация отработавших ?-связей
        for it_cell in self.CellMas.values():
            it_cell.transfer_optimization()

        # формирование ?-связей по оставшемуся неразобранному входу
        v_input_active = i_input_cell_group._get_rest_active()
        if v_input_active.is_none():
            return

        v_inner_active = transfer.CActiveCellsState({}, 1)  # чистый вход

        for it_cell in self.CellMas.values():
            if not it_cell.work__is_current():
                continue

            v_transfer = transfer.CTransfer(
                v_input_active,
                v_inner_active,
                self._TransferParam
            )

            it_cell.add_transfer(v_transfer)

    # ==============================
    def dispatch_input(self, i_input_cell_group: CCellGroup):
        self._dispatch_input_chains_work(i_input_cell_group)
        self._dispatch_input_chain_learn(i_input_cell_group)
        self._dispatch_inner_chain_learn()
        self._dispatch_input_rest(i_input_cell_group)

        for it_cell in self.CellMas.values():
            it_cell.set_transfer_caption(i_input_cell_group, self)

    def reward(self, i_reward: typing.Optional[bool]):
        for itCell in self.CellMas.values():
            itCell.reward(i_reward)

    def stick(self, i_stick: typing.Optional[bool]):
        for itCell in self.CellMas.values():
            itCell.stick(i_stick)

    def get_output(self) -> CState:
        v_out_state = CState(len(self.CellMas), 1)

        for it_cell_idx, it_cell in self.CellMas.items():
            if it_cell.work__is_ready():
                v_out_state.set_item_max(it_cell_idx)

        return v_out_state

    def load(self, i_xml_state):
        if i_xml_state is None:
            return

        v_cells = i_xml_state.getElementsByTagName('Cells')[0]
        v_cell_list = v_cells.getElementsByTagName('Cell')

        v_cells_state = {}
        for s in v_cell_list:
            v_cell_idx = xml.get_int(s, 'Idx')
            v_cells_state[v_cell_idx] = s

        for it_cell_idx, it_cell in self.CellMas.items():
            v_cell_state = None
            if it_cell_idx in v_cells_state:
                v_cell_state = v_cells_state[it_cell_idx]

            it_cell.load(v_cell_state)

        # self.CurrentChain.load(i_state['Series'])
        # self.InitCurrentCell = i_state['Active']
