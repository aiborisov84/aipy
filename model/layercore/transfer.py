from __future__ import annotations
import typing
from dataclasses import dataclass

from . import group
from .state import CRangeValue


# ==============================
class CActiveCellsState:
    # CellIdx -> Level
    CellLevelMap: typing.Mapping[int, int]
    # Количество узлов, формирующих цепочку истории (текущий и предшествующие узлы)
    Range: int

    def is_none(self):
        return not self.CellLevelMap

    def __init__(self, i_state_map: typing.Mapping[int, int], i_range):
        self.CellLevelMap = i_state_map
        self.Range = i_range

    def __repr__(self):
        if not self.CellLevelMap:
            return ''

        return '{}{}'.format(
            f'R{self.Range}' if self.Range > 1 else '',
            self.CellLevelMap
        )

    def is_same(self, i_state: CActiveCellsState):
        return self.CellLevelMap == i_state.CellLevelMap


# ==============================
@dataclass
class CTransferParam:
    Level: int
    ActivationAndInhibition: int
    Strong: int

# ==============================
@dataclass
class CTransferState:
    IsNew: bool
    IsCatch: bool
    IsWork: bool


# ==============================
class CTransfer:
    _Input: CActiveCellsState
    _Inner: CActiveCellsState

    _Learn: CRangeValue
    _Time: CRangeValue
    _Activation: CRangeValue
    _Inhibition: CRangeValue
    _State: CTransferState
    _Caption: str

    def __init__(
            self,
            i_input: CActiveCellsState,
            i_inner: CActiveCellsState,
            i_param: CTransferParam):

        self._Input = i_input
        self._Inner = i_inner
        self._Learn = CRangeValue(i_param.Level)
        self._Learn.set_max()
        self._Time = CRangeValue(i_param.Level)
        self._Time.set_max()
        self._Strong = CRangeValue(i_param.Strong)
        self._Activation = CRangeValue(i_param.ActivationAndInhibition)
        self._Inhibition = CRangeValue(i_param.ActivationAndInhibition)
        self._Caption = ''
        self._State = CTransferState(
            IsNew=True,
            IsWork=True,
            IsCatch=True
        )

    def _get_work_done_str(self):
        v_res = ''
        v_res += '*' if self._State.IsCatch else ' '
        v_res += "+" if self._State.IsNew else '-'

        v_competition = self.get_competition_voice()
        if v_competition > 0:
            v_res += '>'
        elif v_competition == 0:
            v_res += '?'
        else:
            v_res += 'x'

        return v_res

    def __repr__(self):
        return '{} A{} I{} L{} T{} S{} (I:{} C:{}) {}'.format(
            self._get_work_done_str(),
            self._Activation,
            self._Inhibition,
            self._Learn,
            self._Time,
            self._Strong,
            self._Input,
            self._Inner,
            self._Caption
        )

    def get_competition_voice(self):
        return self._Activation.Value - self._Inhibition.Value

    def step(self):
        self._Learn.dec()
        self._Time.dec()

    def is_catch_state(
            self,
            i_input_cell_group: group.CCellGroup,
            i_inner_cell_group: group.CCellGroup):

        if not self._Input.CellLevelMap and not self._Inner.CellLevelMap:
            if not i_inner_cell_group.NoneLevel.is_max():
                return False
        else:
            for it_cell_idx, it_cell_mem_time in self._Input.CellLevelMap.items():
                c_cell_time = i_input_cell_group.CellMas[it_cell_idx].competition__get_time()
                if c_cell_time != it_cell_mem_time:
                    return False

            for it_cell_idx, it_cell_mem_time in self._Inner.CellLevelMap.items():
                c_cell_time = i_inner_cell_group.CellMas[it_cell_idx].competition__get_time()
                if c_cell_time != it_cell_mem_time:
                    return False

        self._State.IsCatch = True

        self._Time.set_max()
        return True

    def work_prepare(self):
        self._State.IsWork = False
        self._State.IsCatch = False
        self._State.IsNew = False

    def work_transfer(
            self,
            i_input_cell_group: typing.Optional[group.CCellGroup],
            i_inner_cell_group: group.CCellGroup):

        if i_input_cell_group is not None:
            for it_cell_idx in self._Input.CellLevelMap.keys():
                vl_link_cell = i_input_cell_group.CellMas[it_cell_idx]
                vl_link_cell.work__transfer()

        for it_cell_idx in self._Inner.CellLevelMap.keys():
            vl_link_cell = i_inner_cell_group.CellMas[it_cell_idx]
            vl_link_cell.work__transfer()

        if self._State.IsNew:
            self._Activation.inc()

        # TODO: Убрал возобновление обучения
        # self._Level.set_max()
        self._State.IsWork = True
        self._Strong.inc()

    def get_active_range(self):
        return self._Inner.Range

    def reward(self):
        if self._Learn.is_set():
            self._Activation.inc()

    def stick(self):
        if self._Learn.is_set():
            self._Inhibition.inc()

    def set_caption(
            self,
            i_input_cell_group: group.CCellGroup,
            i_inner_cell_group: group.CCellGroup):

        def fill_caption_mas(ii_cell_level_map, ii_input_cell_group):
            v_result = []
            for it_cell_idx in ii_cell_level_map.keys():
                vl_link_cell = ii_input_cell_group[it_cell_idx]
                v_caption = vl_link_cell.get_caption()
                if v_caption:
                    v_result.append(v_caption)

            return v_result

        v_input_mas = fill_caption_mas(self._Input.CellLevelMap, i_input_cell_group.CellMas)
        v_inner_mas = fill_caption_mas(self._Inner.CellLevelMap, i_inner_cell_group.CellMas)

        v_full_mas = []
        if v_input_mas:
            v_full_mas.append('I:' + (', '.join(v_input_mas)))
        if v_inner_mas:
            v_full_mas.append('C:' + (', '.join(v_inner_mas)))

        self._Caption = '; '.join(v_full_mas)

    def is_useless(self):
        if self.get_competition_voice() != 0:
            return False

        if self._Learn.is_set():
            return False

        return True

    def is_same(self, i_transfer: CTransfer):
        return (
                self._Inner.is_same(i_transfer._Inner) and
                self._Input.is_same(i_transfer._Input)
        )

