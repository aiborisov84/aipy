from __future__ import annotations
import typing

from .state import CState
from .group import CCellGroup
from .cell import CCellParam
from .transfer import CTransferParam


# ==============================
class CAnalyse:
    GroupMas: typing.List[CCellGroup]

    def __init__(self, i_param, i_state_filename=None):
        self.Param_InCount = i_param['Count']['In']
        self.Param_Link = i_param['Link']
        self.Param_Cell = i_param['Cell']

        c_cell_count = i_param['Count']['Cell']
        c_max_series_length = i_param['Learn']['MaxLength']
        c_cell_level_range = i_param['Cell']['Level']
        c_cell_strong_range = i_param['Cell']['Strong']
        c_action_level_range = i_param['Action']['Level']
        c_action_strong_range = i_param['Action']['Strong']
        c_action_count = i_param['Count']['Action']
        c_link_strong_range = 8  # TODO: i_param['Link']['Strong']
        c_link_level = i_param['Link']['Level']
        c_link_utility = i_param['Link']['Utility']
        c_inner_group_count = i_param['Count']['Inner']

        self.GroupMas = []
        v_size = 2 + c_inner_group_count

        self.GroupMas.append(CCellGroup(
            i_caption=self.group_idx_to_caption(len(self.GroupMas), v_size),
            i_max_chain_length=0,
            i_none_action=False,
            i_cell_count=self.Param_InCount,
            i_cell_param=CCellParam(
                CompetitionTime=1,
                RelaxTime=0,
                StrongRange=1
            ),
            i_transfer_param=CTransferParam(
                Level=1,
                ActivationAndInhibition=c_link_utility,
                Strong=c_link_strong_range
            )
        ))

        for _ in range(c_inner_group_count):
            self.GroupMas.append(CCellGroup(
                i_caption=self.group_idx_to_caption(len(self.GroupMas), v_size),
                i_max_chain_length=c_max_series_length,
                i_none_action=True,
                i_cell_count=c_cell_count,
                i_cell_param=CCellParam(
                    CompetitionTime=c_cell_level_range,
                    RelaxTime=0,
                    StrongRange=c_cell_strong_range
                ),
                i_transfer_param=CTransferParam(
                    Level=1,  # c_link_level,
                    ActivationAndInhibition=c_link_utility,
                    Strong=c_link_strong_range
                )
            ))

        self.GroupMas.append(CCellGroup(
            i_caption=self.group_idx_to_caption(len(self.GroupMas), v_size),
            i_max_chain_length=1,
            i_none_action=(c_inner_group_count <= 0),
            i_cell_count=c_action_count,
            i_cell_param=CCellParam(
                CompetitionTime=2,
                RelaxTime=c_action_level_range,
                StrongRange=c_action_strong_range
            ),
            i_transfer_param=CTransferParam(
                Level=c_link_level,
                ActivationAndInhibition=c_link_utility,
                Strong=c_link_strong_range
            )
        ))

        self.load(i_state_filename)

    # ==============================
    @staticmethod
    def group_idx_to_caption(i_idx: int, i_size: int):
        assert 1 < i_size
        assert 0 <= i_idx < i_size
        if i_idx == 0:
            return 'Input'
        elif i_idx == i_size - 1:
            return 'Output'
        else:
            if i_size == 3:
                return 'Inner'
            else:
                return 'Inner' + str(i_idx)

    # ==============================
    def load(self, i_state_filename):
        if i_state_filename is None:
            return

        from xml.dom import minidom
        v_xml_doc = minidom.parse(i_state_filename)

        v_wn = v_xml_doc.getElementsByTagName('LayerState')[0]
        v_version = v_wn.attributes['Version'].value
        c_current_version = '1.0'
        if v_version != c_current_version:
            raise Exception('Bad version')

        v_groups = v_wn.getElementsByTagName('GroupMas')[0]
        v_group_list = v_groups.getElementsByTagName('Group')

        v_groups_state = {}
        for s in v_group_list:
            v_group_name = s.attributes['Caption'].value
            v_groups_state[v_group_name] = s

        for it_idx, it_item in enumerate(self.GroupMas):
            v_group_name = self.group_idx_to_caption(it_idx, len(self.GroupMas))
            v_group_state = None
            if v_group_name in v_groups_state:
                v_group_state = v_groups_state[v_group_name]

            it_item.load(v_group_state)

    # ==============================
    def step(self, i_input_state, i_reward, i_stick):

        for it_idx, it_item in enumerate(self.GroupMas):
            it_item.step()

        for it_idx, it_item in enumerate(self.GroupMas):
            it_item.reward(i_reward)
            it_item.stick(i_stick)

        self.GroupMas[0].set_state(i_input_state)

        for it_cur, it_next in zip(self.GroupMas, self.GroupMas[1:]):
            it_next.dispatch_input(it_cur)

        return self.GroupMas[-1].get_output()

    # ==============================
    def create_new_input(self):
        return CState(self.Param_InCount, 1)

    # ==============================
    def __repr__(self):
        return '\r\n'.join([str(it) for it in self.GroupMas])


# ==============================
def test():
    v_param = {
        'Count': {'In': 10, 'Cell': 9, 'Action': 3},
        'Cell': {'Level': 10, 'Strong': 8},
        'Link': {'Level': 15, 'Utility': 10},
        'Action': {'Level': 10, 'Strong': 8},
        'Learn': {'MaxLength': 4}
    }
    v_state = {
        'Cell': [[2, 1, False], None, None, None, None, None, None, None, None],
        'Link': {},  # {0: {0: [0,1]}},
        'Action': [None] * 3,
        'Active': None,
        'Series': [0]
    }
    # v_state = None
    v_a = CAnalyse(v_param, v_state)

    # noinspection PyProtectedMember
    def step(i_input=None):
        if i_input is not None:
            print(i_input)
            v_a.step(i_input, None, None)

        print(v_a)

    step()
    v_input = v_a.create_new_input()
    step(v_input)
    v_input.Mas[1].set_max()
    v_input.Mas[2].set_max()
    step(v_input)
