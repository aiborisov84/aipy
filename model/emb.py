import enum
from abc import ABC, abstractmethod
from itertools import chain
import platform
from PyQt5.QtGui import QFont


GIndent: str = ' '*4  # '\t'


def do_format(i_data):

    if isinstance(i_data, dict):
        def dict_format(i_key, ii_data):
            if i_key == "":
                return str(ii_data)
            else:
                return f"{i_key}: {ii_data}"

        return ('\r\n'+GIndent).join(chain([''], [dict_format(k, v) for k, v in i_data.items()]))
    else:
        return ('\r\n'+GIndent).join(chain([''], map(str, i_data)))


GIsDroid = platform.machine().startswith('arm') or platform.machine().startswith('aarch')


def get_appl_font():
    if GIsDroid:
        return QFont('Droid Sans Mono', 16)
    else:
        return QFont('Courier New', 16)  # 'Segoe UI'12


def get_log_font():
    if GIsDroid:
        return QFont('Droid Sans Mono', 6)
    else:
        return QFont('Courier New', 8)


# ==============================
class State(enum.Enum):
    Input = 0
    Inner = 1
    Output = 2


# ==============================
class IDynamic(ABC):
    """Динамический блок"""

    @abstractmethod
    def do_step(self):
        pass


# ==============================
class IDetector(IDynamic):
    """Динамический детектор"""

    @abstractmethod
    def is_start(self, i_tick_idx, i_input):
        pass


# ==============================
class ISampleDetector(IDetector):
    """Динамический образец с стартом по детектору"""

    @abstractmethod
    def get_current(self):
        pass


# ==============================
class ISample(IDynamic):
    """Динамический образец с ручным стартом"""

    @abstractmethod
    def set_start(self):
        pass


# ==============================
class IModule(ABC):
    """Интерфейс модуля среды. Должен быть реализован в классе с именем CModule"""

    @abstractmethod
    def get_param(self):
        pass

    @abstractmethod
    def step(self, i_tick_idx, i_input):
        pass

    @abstractmethod
    def get_component(self):
        pass


# ==============================
# ==============================
class CDetectItem:
    def __init__(self, *i_arg_mas):
        self.Mas = [*i_arg_mas]
        while len(self.Mas) > 0 and self.Mas[-1] is None:
            self.Mas.pop()

    def is_same(self, i_input):
        if len(i_input) < len(self.Mas):
            return False

        for itDetect, itInput in zip(self.Mas, i_input):
            if itDetect is None:
                continue
            if itDetect != itInput:
                return False

        return True


# ==============================
class CDetector(IDetector):
    def __init__(self, i_detect_mas):
        self.Idx = None
        self._DetectMas = i_detect_mas
        self._DetectInLast = False

    def is_start(self, i_tick_idx, i_input):
        if self.Idx is None:
            self.Idx = 0
        else:
            if len(self._DetectMas) <= self.Idx:
                self.Idx = None
                self._DetectInLast = True
                return True

        if not self._DetectMas[self.Idx].is_same(i_input):
            self.Idx = None

        return False

    def do_step(self):
        if self.Idx is not None:
            self.Idx = self.Idx + 1

        self._DetectInLast = False

    @property
    def is_detect_in_last(self):
        return self._DetectInLast


# ==============================
# ==============================
class CSample(ISample):
    def __init__(self, i_out_mas):
        self.Idx = None
        self._OutMas = i_out_mas
        self._IsStart = False

    def do_step(self):
        if self.Idx is not None:
            self.Idx = self.Idx + 1
            if len(self._OutMas) <= self.Idx:
                self.Idx = None

    def get_current(self):
        if self.Idx is None:
            if self._IsStart:
                self.Idx = 0
                self._IsStart = False
            else:
                return None

        return self._OutMas[self.Idx]

    def set_start(self):
        self._IsStart = True


# ==============================
class CSampleDetector(ISampleDetector):
    def __init__(self, i_out_mas, i_out_none, i_is_start):
        self.Idx = None
        self._OutMas = i_out_mas
        self._OutNone = i_out_none
        self._IsStart = i_is_start

    def is_start(self, i_tick_idx, i_input):
        if self._IsStart(i_tick_idx, i_input) and self.Idx is None:
            self.Idx = 0
            return True
        else:
            return False

    def do_step(self):
        if self.Idx is not None:
            self.Idx = self.Idx + 1
            if len(self._OutMas) <= self.Idx:
                self.Idx = None

    def get_current(self):
        if self.Idx is None:
            return self._OutNone
        else:
            return self._OutMas[self.Idx]
