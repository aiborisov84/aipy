# ==============================
def get_int(i_xml_node, i_name: str):
    return int(i_xml_node.attributes[i_name].value)


# ==============================
def get_int_optional(i_xml_node, i_name: str, i_def: int):
    return int(get_optional(i_xml_node, i_name, i_def))


# ==============================
def get_optional(i_xml_node, i_name: str, i_def=None):
    if i_name in i_xml_node.attributes:
        return i_xml_node.attributes[i_name].value
    else:
        return i_def
