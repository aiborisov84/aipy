from .item import CBlock
from . import emb
from .layercore.analyse import CAnalyse
import logging
from . import xml
import glob
import os.path as op
from PyQt5.QtGui import QFont
from PyQt5.QtQml import QQmlComponent, QQmlEngine
from PyQt5.QtCore import QObject, pyqtSlot, pyqtProperty, pyqtSignal, QUrl, QCoreApplication  # , QTimer


class PyApplicationData(QObject):
    onInputChanged = pyqtSignal()
    onInnerChanged = pyqtSignal()
    onOutputChanged = pyqtSignal()
    _inner_group_count: int

    def __init__(self, i_inner_group_count):
        QObject.__init__(self)
        self._inner_group_count = i_inner_group_count
        self._status_text = ""
        self._text_input = ""
        self._text_inner = ""
        self._text_output = ""

    @pyqtProperty(str, notify=onInputChanged)
    def textInput(self):
        return self._text_input

    @pyqtProperty(str, notify=onInnerChanged)
    def textInner(self):
        return self._text_inner

    @pyqtProperty(str, notify=onOutputChanged)
    def textOutput(self):
        return self._text_output

    @pyqtSlot(result=QFont)
    def getLogFont(self):
        return emb.get_log_font()

    @pyqtSlot(result=int)
    def getInnerCount(self):
        return self._inner_group_count

    def set_status_text(
            self,
            i_input_text: str,
            i_inner_text: str,
            i_output_text: str):
        self._text_input = i_input_text
        self.onInputChanged.emit()

        self._text_inner = i_inner_text
        self.onInnerChanged.emit()

        self._text_output = i_output_text
        self.onOutputChanged.emit()


# ==============================
class CLayer(CBlock):
    def __init__(self, i_params):
        super().__init__(i_params)
        self._InnerIdx = 0
        c_lines = i_params['ItemLines']
        v_input_count = c_lines[emb.State.Input]['']
        self._OutputCount = 0
        self._OutputMap = []
        for itName, itCount in c_lines[emb.State.Output].items():
            self._OutputCount += itCount
            for itIdx in range(itCount):
                self._OutputMap.append((itName, itIdx))

        c_inner_group_count = i_params['Analyse']['InnerGroupCount']

        v_analyse_param = {
            'Count': {
                'In': v_input_count,
                'Inner': i_params['Analyse']['InnerGroupCount'],
                'Cell': i_params['Analyse']['CellCount'],
                'Action': self._OutputCount
            },
            'Cell': i_params['Analyse']['Cell'],
            'Link': i_params['Analyse']['Link'],
            'Action': i_params['Analyse']['Action'],
            'Learn': i_params['Analyse']['Learn']
        }

        v_state_caption = i_params['Analyse']['State']['Caption']
        v_state_dir = i_params['Analyse']['State']['Dir']
        v_state_list = glob.glob(op.join(v_state_dir, v_state_caption+'*.state'))
        print('\r\n'.join(v_state_list))
        v_def_state_filename = None
        if v_state_list:
            v_def_state_filename = v_state_list[0]

        self.Analyse = CAnalyse(v_analyse_param, v_def_state_filename)

        if QCoreApplication.instance():
            self._QmlEngine = QQmlEngine()
            self._qml_data = PyApplicationData(c_inner_group_count)
            self._QmlEngine.rootContext().setContextProperty(
                "applicationData", self._qml_data)

            v_dir = op.dirname(__file__)
            self._QmlEngine.addImportPath(v_dir)
            self._QmlEngine.setBaseUrl(QUrl("file:///" + v_dir+"/"))

            self._component = QQmlComponent(self._QmlEngine)
            self._component.loadUrl(QUrl("Layer.qml"))
            if self._component.status() != QQmlComponent.Ready:
                self._component = None

    # ==============================
    @classmethod
    def create_from_xml(cls, i_xml_item, i_dir):
        v_params = super().param_from_xml(i_xml_item)
        v_version = i_xml_item.attributes['Version'].value

        c_current_version = '3.0'
        if v_version != c_current_version:
            raise Exception('Bad version')

        v_caption = i_xml_item.attributes['Caption'].value

        v_inputs = i_xml_item.getElementsByTagName('Input')
        v_outputs = i_xml_item.getElementsByTagName('Output')
        v_data = i_xml_item.getElementsByTagName('Data')[0]

        v_item_lines = {emb.State.Inner: {'': 0}}

        v_input_sizes = {}
        for itInput in v_inputs:
            v_in_name = itInput.attributes['Name'].value
            v_in_size = xml.get_int(itInput, 'Size')
            v_input_sizes[v_in_name] = v_in_size

        v_item_lines[emb.State.Input] = v_input_sizes

        v_output_sizes = {}
        for itOutput in v_outputs:
            v_out_name = itOutput.attributes['Name'].value
            v_out_size = xml.get_int(itOutput, 'Size')
            v_output_sizes[v_out_name] = v_out_size

        v_item_lines[emb.State.Output] = v_output_sizes

        v_cell = v_data.getElementsByTagName('Cell')[0]
        v_link = v_data.getElementsByTagName('Link')[0]
        v_action = v_data.getElementsByTagName('Action')[0]
        v_learn = v_data.getElementsByTagName('Learn')[0]
        v_a = {
            'State': {
                'Caption': v_caption,
                'Dir': i_dir
            },
            'InnerGroupCount': xml.get_int_optional(v_cell, 'InnerGroupCount', 1),
            'CellCount': xml.get_int(v_cell, 'Count'),
            'Cell': {
                'Level': xml.get_int(v_cell, 'Level'),
                'Strong': xml.get_int(v_cell, 'Strong')
            },
            'Link': {
                'Level': xml.get_int(v_link, 'Level'),
                'Utility': xml.get_int(v_link, 'Utility')
            },
            'Action': {
                'Level': xml.get_int(v_action, 'Level'),
                'Strong': xml.get_int(v_action, 'Strong')
            },
            'Learn': {
                'MaxLength': xml.get_int(v_learn, 'MaxLength')
            }
        }

        v_params['ItemLines'] = v_item_lines
        # v2 v_params['Data'] = v_data.firstChild.nodeValue
        v_params['Analyse'] = v_a

        return cls(v_params)

    # ==============================
    def do_tick(self):
        self._InnerIdx = self._InnerIdx + 1

        v_input = self.Analyse.create_new_input()

        c_in_lines = self._State[emb.State.Input]

        c_in_data = c_in_lines['']

        def get_special_input(i_name):
            v_res = None
            if i_name in c_in_lines:
                c_in_special = c_in_lines[i_name]
                if len(c_in_special) > 0:
                    if c_in_special[0] > 0:
                        v_res = True
                    else:
                        v_res = False
                        
            return v_res

        c_reward = get_special_input('Reward')
        c_stick = get_special_input('Stick')

        for it, itData in enumerate(c_in_data):
            if itData > 0:
                v_input.Mas[it].set_max()

        v_in_str = f'In: {v_input} Reward={c_reward} Stick={c_stick}'

        v_output = self.Analyse.step(v_input, c_reward, c_stick)
        v_analyse_str = str(self.Analyse)

        vl_out = self._State[emb.State.Output]
        for itIdx, itOutLink in enumerate(self._OutputMap):
            v_out_name = itOutLink[0]
            v_out_idx = itOutLink[1]
            vl_out[v_out_name][v_out_idx] = 1 if v_output.Mas[itIdx].is_set() else 0

        self._qml_data.set_status_text(
            ("Tick: {}\r\n".format(self._InnerIdx)) +
            str(self.Analyse.GroupMas[0]) +
            ("\r\nReward" if c_reward else "") +
            ("\r\nStick" if c_stick else ""),
            str(self.Analyse.GroupMas[1]) if len(self.Analyse.GroupMas) > 2 else "",
            str(self.Analyse.GroupMas[-1])
        )

        logging.info(
            '\r\n'.join([
                '[====== Layer',
                v_in_str,
                v_analyse_str,
                str(vl_out),
                ']====== Layer'
            ])
        )

    # ==============================
    def get_component(self):
        return self._component
