from abc import ABC, abstractmethod
from . import emb


# ==============================
class CItem(ABC):
    def __init__(self, i_params):
        self._DrawItem = None
        self.Position = None
        self.Caption = i_params['Caption']
        
    @classmethod
    def param_from_xml(cls, i_xml_item):
        v_params = {}
        v_caption = i_xml_item.attributes['Caption'].value
        v_params['Caption'] = v_caption
        return v_params
        
    @abstractmethod
    def save(self, i_parent_node):
        pass


# ==============================
class CBlock(CItem):
    def __init__(self, i_params):
        super().__init__(i_params)
        self.BlockId = i_params['BlockId']
        
        v_state = {}
        
        for itType, itLineSet in i_params['ItemLines'].items():
            v_line_set_state = {}
            for itName, itSize in itLineSet.items():
                v_line_set_state[itName] = [0] * itSize
            v_state[itType] = v_line_set_state
            
        self._State = v_state
        
    # ==============================
    @classmethod
    def param_from_xml(cls, i_xml_item):
        v_params = super().param_from_xml(i_xml_item)
        v_block = i_xml_item.getElementsByTagName('Block')[0]
        v_block_id = v_block.attributes['ID'].value
        v_params['BlockId'] = v_block_id
        return v_params
        
    # ==============================
    def save(self, i_parent_node):
        pass
        
    # ==============================
    def set_input(self, i_to_line_idx, i_line_data):
        vl_input = self._State[emb.State.Input]
        vl_input_set = vl_input[i_to_line_idx.Name]
        for itIdx, itValue in enumerate(i_line_data):
            vl_input_set[i_to_line_idx.StartIdx + itIdx] = itValue

    # ==============================
    def get_output(self, i_from_line_idx, i_line_count):
        v_result = [0] * i_line_count
        
        v_out_set = self._State[emb.State.Output]
        v_out = v_out_set[i_from_line_idx.Name]
        for it in range(i_line_count):
            v_result[it] = v_out[it + i_from_line_idx.StartIdx]
            
        return v_result
    
    # ==============================
    @abstractmethod
    def do_tick(self):
        pass

