import QtQuick.Controls 2.0
import QtQuick 2.11
import QtQuick.Shapes 1.11
import QtQuick.Controls 1.4 as C


Page {
    property string contentText
    property int historyLength: 5
    property font textFont

    ListModel {
        id: historyList
    }

    onContentTextChanged: {
        //console.info(contentText)
        cmptTxt.text = contentText
        historyList.insert(0, {text: contentText})
        if (historyLength < historyList.count)
            historyList.remove(historyList.count-1)
    }

    onTextFontChanged: {
        cmptTxt.font = textFont
        historyLabel.font = textFont
        historyItem.font = textFont
    }

    header: Row {
    	Text {
            id: historyLabel
            leftPadding: 5
            height: historyItem.height
            verticalAlignment: Qt.AlignVCenter
            text: "Hist."
        }
        
        SpinBox {
            id: historyItem
            value: 0
            from: 0
            to: historyLength-1

            background: Rectangle {
                id: bg
                visible: false
                width: historyItem.width
                height: historyItem.height
            }

            onValueChanged: {
                if(value < historyList.count)
                    cmptTxt.text = historyList.get(value).text
                else
                    cmptTxt.text = ""
            }
        }
    }

    Flickable {
        property alias textFont: cmptTxt.font

        anchors.fill: parent

        id: flicArea
        clip: true
        TextArea.flickable: TextArea {
            id: cmptTxt
            readOnly: true
            //selectByMouse: true
            textFormat: TextEdit.PlainText
            wrapMode: TextEdit.NoWrap
        }

        ScrollBar.vertical: ScrollBar {}
        ScrollBar.horizontal: ScrollBar {}

        property int lastY: 0
        property bool userChage: false

        onContentYChanged: {
            if(!userChage)
                if(contentY!=lastY)
                    contentY=lastY
        }

        onMovementStarted: {
            userChage = true
        }

        onMovementEnded: {
            userChage = false
            lastY = contentY
        }
    }
}
