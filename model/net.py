from .item import CBlock
from .environment import CEnvironment
from .layer import CLayer
from .connection import CConnection
import os.path as op
import typing


# ==============================
class CNet:
    BlockMap: typing.Mapping[int, CBlock]
    LayerMas: typing.List[CLayer]
    EnvMas: typing.List[CEnvironment]
    ConnectionMas: typing.List[CConnection]

    def __init__(
            self,
            i_block_map: typing.Mapping[int, CBlock],
            i_connection_mas: typing.List[CConnection],
            i_env_mas: typing.List[CEnvironment],
            i_layer_mas: typing.List[CLayer]):

        self.BlockMap = i_block_map
        self.ConnectionMas = i_connection_mas
        self.EnvMas = i_env_mas
        self.LayerMas = i_layer_mas
        v_keys = self.BlockMap.keys()
        self._NextID = max(v_keys) + 1
        self.CurrentTime = 0
    
    # ==============================    
    @classmethod
    def create_from_xml(cls, i_xml_tree, i_dir):
        v_wn = i_xml_tree.getElementsByTagName('WorkNet')[0]
        v_version = v_wn.attributes['Version'].value
        c_current_version = '1.0'
        if v_version != c_current_version:
            raise Exception('Bad version')
            
        v_blocks = v_wn.getElementsByTagName('Blocks')[0]
        v_environments = v_blocks.getElementsByTagName('Environments')[0]
        v_environment_list = v_environments.getElementsByTagName('Environment')
        v_layers = v_blocks.getElementsByTagName('Layers')[0]
        v_layer_list = v_layers.getElementsByTagName('Layer')
        v_connections = v_wn.getElementsByTagName('Connections')[0]
        v_connection_list = v_connections.getElementsByTagName('Connection')
        
        v_block_map = {}
        v_connection_mas = []
        v_env_mas = []
        v_layer_mas = []
        
        for s in v_environment_list:
            v_environment = CEnvironment.create_from_xml(s, i_dir)
            v_block_map[int(v_environment.BlockId)] = v_environment
            v_env_mas.append(v_environment)
            
        for s in v_layer_list:
            v_layer = CLayer.create_from_xml(s, i_dir)
            v_block_map[int(v_layer.BlockId)] = v_layer
            v_layer_mas.append(v_layer)
            
        for s in v_connection_list:
            v_connection = CConnection.create_from_xml(
                s,
                lambda i_block_id: v_block_map[i_block_id]
            )
            v_connection_mas.append(v_connection)

        return cls(v_block_map, v_connection_mas, v_env_mas, v_layer_mas)
   
    # ==============================    
    @classmethod
    def create_from_file(cls, i_file):
        from xml.dom import minidom
        v_dir = op.dirname(i_file)
        v_xml_doc = minidom.parse(i_file)
        return cls.create_from_xml(v_xml_doc, v_dir)
        
    # ==============================    
    def do_tick(self):
        self.CurrentTime = self.CurrentTime + 1

        for _, itItem in self.BlockMap.items():
            itItem.do_tick()
        
        for itItem in self.ConnectionMas:
            itItem.do_transmit()

    # ==============================
    def get_env_components(self):
        return [it.get_component() for it in self.EnvMas if it.get_component() is not None]

    # ==============================
    def get_layer_components(self):
        return [it.get_component() for it in self.LayerMas if it.get_component() is not None]


# ==============================
def test(i_file):
    v_net = CNet.create_from_file(i_file)
    for _ in range(30):
        v_net.do_tick()
