from .item import CBlock
from . import emb
import os.path as op
import importlib.util
import sys
import logging
from PyQt5.QtGui import QFont
from PyQt5.QtQml import QQmlComponent, QQmlApplicationEngine
from PyQt5.QtCore import QObject, pyqtSlot, pyqtProperty, pyqtSignal, QUrl, QCoreApplication


class PyApplicationData(QObject):
    onEnvChanged = pyqtSignal()

    def __init__(self, i_custom_component):
        QObject.__init__(self)
        self._status_text = ""
        self._text_env = ""
        self._custom_component = i_custom_component

    @pyqtProperty(str, notify=onEnvChanged)
    def textEnv(self):
        return self._text_env

    @pyqtSlot(result=QFont)
    def getLogFont(self):
        return emb.get_log_font()

    @pyqtSlot(result=QQmlComponent)
    def getCustom(self):
        return self._custom_component

    def set_status_text(self, i_env_text: str):
        self._text_env = i_env_text
        self.onEnvChanged.emit()


# ==============================
class CEnvironment(CBlock):
    _Module: emb.IModule

    def __init__(self, i_param):
        super().__init__(i_param)
        
        self.CurrentTick = 0
        self._Module = i_param['Module']

        if QCoreApplication.instance():
            v_custom_component = None
            if self._Module:
                v_custom_component = self._Module.get_component()

            self._QmlEngine = QQmlApplicationEngine()
            self._qml_data = PyApplicationData(v_custom_component)
            self._QmlEngine.rootContext().setContextProperty(
                "applicationData", self._qml_data)

            v_dir = op.dirname(__file__)
            self._QmlEngine.addImportPath(v_dir)
            self._QmlEngine.setBaseUrl(QUrl("file:///" + v_dir+"/"))

            self._component = QQmlComponent(self._QmlEngine)
            self._component.loadUrl(QUrl("Environment.qml"))
            if self._component.status() != QQmlComponent.Ready:
                self._component = v_custom_component
    
    # ==============================
    @classmethod
    def create_from_xml(cls, i_xml_item, i_dir):
        v_params = super().param_from_xml(i_xml_item)
        v_version = i_xml_item.attributes['Version'].value
        
        c_current_version = '2.0'
        if v_version != c_current_version:
            raise Exception('Bad version')
        
        v_data = i_xml_item.getElementsByTagName('Data')[0]
        v_script_file = v_data.attributes['ScriptFile'].value
        
        v_file = op.join(i_dir, v_script_file)
        v_module_name = op.splitext(v_script_file)[0]
        
        sys.path.insert(0, i_dir)
        v_spec = importlib.util.spec_from_file_location(v_module_name, v_file)
        v_py_module = importlib.util.module_from_spec(v_spec)
        v_spec.loader.exec_module(v_py_module)
        v_module = v_py_module.CModule()

        assert isinstance(v_module, emb.IModule)

        v_params['ItemLines'] = v_module.get_param()
        v_params['Module'] = v_module
             
        return cls(v_params)
    
    # ==============================
    def do_tick(self):
        v_input = self._State[emb.State.Input]
        v_inner, v_out = self._Module.step(
            self.CurrentTick, 
            v_input
        )
        self._State[emb.State.Inner] = v_inner
        self._State[emb.State.Output] = v_out

        v_env_str = '\r\n'.join([
            'Tick: {}'.format(self.CurrentTick),
            'In: {}'.format(emb.do_format(v_input)),
            'Inner: {}'.format(emb.do_format(v_inner)),
            'Out: {}'.format(emb.do_format(v_out)),
        ])

        self._qml_data.set_status_text('\r\n'.join([
            'Environment [',
            v_env_str,
            ']'
        ]))

        logging.info('\r\n'.join([
            '[====== Environment',
            v_env_str,
            ']====== Environment'
        ]))

        self.CurrentTick = self.CurrentTick+1

    # ==============================
    def get_component(self):
        return self._component
