import QtQuick.Controls 2.0
import QtQuick 2.11
import QtQuick.Shapes 1.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 1.4 as C

Item {
    property Item envCustomItem: null

    anchors.fill: parent

    C.SplitView {
        anchors.fill: parent
        orientation: Qt.Vertical

        ItemTextView {
            width: parent.width
            height: viewEnvCustom.visible ? parent.height*0.5 : parent.height
            textFont: applicationData.getLogFont()
            contentText: applicationData.textEnv
        }

        Page {
            width: parent.width
            height: parent.height*0.5
            id: viewEnvCustom
        }
    }

    function setCustom(iComponent)
    {
        if(envCustomItem)
        {
            envCustomItem.destroy()
            envCustomItem = null
        }

        if(iComponent)
        {
            console.log("Ready CustomEnvironment Insert!")
            envCustomItem = iComponent.createObject(viewEnvCustom, {"x": 0, "y": 0});
            viewEnvCustom.visible = true
        }
        else
        {
            viewEnvCustom.visible = false
        }
    }

    Component.onCompleted: {
        console.log("Completed Environment Running!")

        setCustom(applicationData.getCustom())
    }
}
