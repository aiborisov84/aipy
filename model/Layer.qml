import QtQuick.Controls 2.0
import QtQuick 2.11
import QtQuick.Shapes 1.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 1.4 as C

Item {
    anchors.fill: parent

    ListModel {
        id: innerList
    }

    C.SplitView {
        anchors.fill: parent
        orientation: Qt.Horizontal

        ItemTextView {
            width: parent.width*0.2
            height: parent.height
            textFont: applicationData.getLogFont()
            contentText: applicationData.textInput
        }

        StackLayout {
            id: swipeViewInner
            width: parent.width*0.45
            height: parent.height

            Page {
                id: viewInnerTab

                StackLayout {
                    id: swipeView
                    anchors.fill: parent
                    currentIndex: tabBar.currentIndex

                    Repeater {
                        model: innerList
                        delegate: ItemTextView {
                            width: parent.width*0.45
                            height: parent.height
                            textFont: applicationData.getLogFont()
                            contentText: applicationData.textInner
                        }
                    }
                }

                footer: TabBar {
                    id: tabBar

                    Repeater {
                        model: innerList
                        TabButton {
                            text: item_title
                        }
                    }
                }
            }
        }

        ItemTextView {
            width: parent.width*0.35
            height: parent.height
            textFont: applicationData.getLogFont()
            contentText: applicationData.textOutput
        }
    }

    Component.onCompleted: {
        console.log("Completed Layer Running!")

        for(var i=0; i<applicationData.getInnerCount(); i++)
        {
            innerList.append(
            {
                "content": (i).toString(),
                "item_title": "Inner"+(i+1).toString()
            })
        }
        swipeViewInner.visible = (innerList.count > 0)
    }
}
