# Общая теория алгоритмов. Моделирование процесса запоминания и обучения
## Лицензия
```
 GNU AFFERO GENERAL PUBLIC LICENSE
 Version 3, 19 November 2007
```

Полный текст лицензии ([LICENSE](https://bitbucket.org/aibor/ai_borisov_cit/wiki/LICENSE.txt))

## Контактная информация
Авторы: 

- Борисов Алексей Игоревич (aiborisov84@gmail.com).
([Доска публикаций Хабр](https://habr.com/ru/users/ai_borisov/))

## Вики

- [Описание](https://bitbucket.org/aibor/ai_borisov_cit/wiki/Home)